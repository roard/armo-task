﻿
namespace ARMO_task.Interfaces
{
    public interface IView
    {
        void Show();
        void Close();
    }
}
