﻿
namespace ARMO_task.Interfaces
{
    public interface IPresenter
    {
        void Run();
    }
}
