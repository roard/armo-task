﻿using System;
using System.Windows.Forms;

using ARMO_task.Presenters;
using ARMO_task.Models;

namespace ARMO_task
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainFormPresenter presenter = new MainFormPresenter(new MainForm(), new MainFormModel());
            presenter.Run();
        }
    }
}
