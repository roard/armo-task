﻿using ARMO_task.Interfaces;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ARMO_task.Models
{
    class MainFormModel : IMainFormModel
    {
        private readonly string FNAME = "sparams.dat";
        private readonly string thisAppDir;
        private BinaryFormatter formater = new BinaryFormatter();

        public MainFormModel()
        {
            thisAppDir = Directory.GetCurrentDirectory();
        }

        #region Public methods
        /// <summary>
        /// Читает из sparams.dat сохраненные параметры поиска
        /// </summary>
        /// <returns>Успешно считанное значение либо null</returns>
        public MainFormSearchParams ReadSearchParams()
        {
            MainFormSearchParams result = null;

            Stream serialStream = new FileStream(FNAME, FileMode.Open);
            if(serialStream != null)
            {
                try
                {
                    result = (MainFormSearchParams)formater.Deserialize(serialStream);
                }
                catch
                { };
            }
            serialStream.Close();

            return result;
        }

        /// <summary>
        /// Записывает в sparams.dat параметры поиска
        /// </summary>
        /// <param name="sParams">Значение для записи в файл</param>
        public void WriteSearchParams(MainFormSearchParams sParams)
        {
            if(sParams != null)
            {
                Stream serialStream = new FileStream(FNAME, FileMode.Truncate);
                formater.Serialize(serialStream, sParams);
                serialStream.Close();
            }
        }
        #endregion
    }
}
