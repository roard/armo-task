﻿using System;
using ARMO_task.Utility;

namespace ARMO_task.Interfaces
{
    public enum MainFormGUIState
    { ReadyForSearch, Searching, Paused, SearchEnded }

    public interface IMainFormView: IView
    {
        #region Properties
        DirectorySearcher DirectorySearcher { get; }
        MainFormSearchParams SearchParams { get; set; }
        #endregion

        #region Events
        event Action CommandSearchStart;
        event Action CommandSearchPause;
        event Action CommandSearchResume;
        #endregion

        #region External events handling
        void OnFileProcess(string str);
        void OnNewSearchResult(string str);
        void GUIStateChange(MainFormGUIState state);
        #endregion

        void ShowError(string err);
    }
}
