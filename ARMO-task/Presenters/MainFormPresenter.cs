﻿using ARMO_task.Interfaces;
using ARMO_task.Utility;

namespace ARMO_task.Presenters
{
    class MainFormPresenter : IPresenter
    {
        private readonly IMainFormView _mainFormView;
        private readonly IMainFormModel _mainFormModel;

        private readonly DirectorySearcher _dirSearcher;

        public MainFormPresenter(IMainFormView mFormView, IMainFormModel mFormModel)
        {
            #region init
            _mainFormView = mFormView;
            _mainFormModel = mFormModel;

            _dirSearcher = _mainFormView.DirectorySearcher;
            #endregion
            
            #region subscribes
            _mainFormView.CommandSearchStart += () => SearchStart(_mainFormView.SearchParams);
            _mainFormView.CommandSearchPause += SearchPause;
            _mainFormView.CommandSearchResume += SearchResume; 

            _dirSearcher.SearchStarted += () => _mainFormView.GUIStateChange(MainFormGUIState.Searching);
            _dirSearcher.SearchPaused += () => _mainFormView.GUIStateChange(MainFormGUIState.Paused);
            _dirSearcher.SearchResumed += () => _mainFormView.GUIStateChange(MainFormGUIState.Searching);
            _dirSearcher.SearchEnded += () => _mainFormView.GUIStateChange(MainFormGUIState.SearchEnded);

            _dirSearcher.FileFound += _mainFormView.OnNewSearchResult;
            _dirSearcher.FileProceed += _mainFormView.OnFileProcess;
            #endregion

            #region app params reading
            _mainFormView.SearchParams = _mainFormModel.ReadSearchParams();
            #endregion
        }

        public void Run()
        {
            _mainFormView.Show();
        }

        #region MainForm commands handling
        /// <summary>
        /// Записывает полученные параметры поиска в модель, запускает по ним новый поиск 
        /// </summary>
        /// <param name="searchParams">Параметры поиска</param>
        private void SearchStart(MainFormSearchParams searchParams)
        {
            if(searchParams != null)
            {
                _mainFormModel.WriteSearchParams(searchParams);

                string path = searchParams.Path;
                string fname = searchParams.Filename;
                string pattern = searchParams.InnerPattern;

                _dirSearcher.NewSearch(path, fname, pattern);
            }
        }

        /// <summary>
        /// Продолжает проистановленный поиск
        /// </summary>
        private void SearchResume()
        {
            _dirSearcher.ResumeSearch();
        }

        /// <summary>
        /// Приостанавливает текущий поиск
        /// </summary>
        private void SearchPause()
        {
            _dirSearcher.PauseSearch();
        }
        #endregion
    }
}
