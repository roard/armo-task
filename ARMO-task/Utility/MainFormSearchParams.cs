﻿using System;

namespace ARMO_task
{
    /// <summary>
    /// Формат хранения параметров поиска из главной формы
    /// </summary>
    [Serializable()]
    public class MainFormSearchParams
    {
        public MainFormSearchParams(string path, string fname, string pattern)
        {
            Path = System.IO.Path.GetFullPath(path);
            Filename = fname;
            InnerPattern = pattern;
        }

        #region Properties
        public string Path { get; private set; }

        public string Filename { get; private set; }

        public string InnerPattern { get; private set; }
        #endregion
    }
}
