﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ARMO_task.Utility
{
    /// <summary>
    /// Контрол для поиска в определенной директории по имени файла и его текстовому содержимому.
    /// </summary>
    public class DirectorySearcher : Control
    {
        private readonly ManualResetEvent _mre = new ManualResetEvent(false);
        private CancellationTokenSource _tokenSource = new CancellationTokenSource();

        #region Events
        public delegate void TextDelegate(string path);
        public event TextDelegate FileProceed;
        public event TextDelegate FileFound;

        public Action SearchStarted;
        public Action SearchPaused;
        public Action SearchResumed;
        public Action SearchEnded;
        #endregion

        #region Public methods
        /// <summary>
        /// Начинает новый асинхронный поиск; убивает текущий, если есть.
        /// </summary>
        /// <param name="searchPath">Директория, в которой будет выполняться поиск</param>
        /// <param name="searchFilename">Шаблон имени файла, который ищем</param>
        /// <param name="searchPattern">Отрывок текста, содержащийся в файле, который ищем</param>
        public void NewSearch(string searchPath, string searchFilename, string searchPattern)
        {
            _tokenSource.Cancel();
            _tokenSource = new CancellationTokenSource();

            SearchStarted?.Invoke();

            _mre.Set();
            Thread searchThread = new Thread(() => TreadProc(searchPath, searchFilename, searchPattern, _mre, _tokenSource.Token));
            searchThread.Start();
        }
        /// <summary>
        /// Приостанавливает идущий поиск. Отстреливает событие SearchPaused.
        /// </summary>
        public void PauseSearch()
        {
            if (SearchPaused != null) this.Invoke(SearchPaused);

            _mre.Reset();
        }

        /// <summary>
        /// Возобновляет приостановленный поиск. Отстреливает событие SearchResumed.
        /// </summary>
        public void ResumeSearch()
        {
            if (SearchResumed != null) this.Invoke(SearchResumed);

            _mre.Set();
        }
        #endregion


        #region Private methods
        /// <summary>
        /// Делегат для асинхронного поиска. По окончании поиска отстреливает событие SearchEnded. Ловит ошибки, выкидывает их наружу.
        /// </summary>
        private void TreadProc(string searchPath, string searchFilename, string searchPattern, ManualResetEvent mre, CancellationToken token)
        {
            try
            {
                RecursiveSearch(searchPath, searchFilename, searchPattern, _mre, _tokenSource.Token);
            }
            catch(Exception e)
            {
                throw e;
            }
            finally
            {
                if(SearchEnded != null) this.Invoke(SearchEnded);
            }
        }

        /// <summary>
        /// Выполняет рекурсивный обход переданной директории, сравнивая имена файлов с шаблоном. При полном совпадении имени файла с шаблоном читает файл как текстовый, ищет внутри наличие текста.
        /// Диспатчит найденные и обрабатываемые файлы.
        /// </summary>
        /// <param name="searchPath">Директория, в которой будет выполняться поиск</param>
        /// <param name="searchFilename">Шаблон имени файла, который ищем</param>
        /// <param name="searchPattern">Отрывок текста, содержащийся в файле, который ищем</param>
        /// <param name="mre">ManualResetEvent для приостановки ищущего потока</param>
        /// <param name="token">CancellationToken для отмены таска</param>
        private void RecursiveSearch(string searchPath, string searchFilename, string searchPattern, ManualResetEvent mre, CancellationToken token)
        {
            string[] filePaths = { };
            try
            {
                filePaths = Directory.GetFiles(searchPath);
            }
            catch (System.UnauthorizedAccessException)
            { };

            foreach (var filePath in filePaths)
            {
                mre.WaitOne();
                if (token.IsCancellationRequested)
                    break;

                if (FileProceed != null) this.Invoke(FileProceed, new object[] { filePath });

                string fname = Path.GetFileName(filePath);
                string fnameShort = Path.GetFileNameWithoutExtension(filePath);

                if (fname == searchFilename || fnameShort == searchFilename
                    && File.ReadAllText(filePath, Encoding.Default).Contains(searchPattern))
                {
                    if (FileFound != null) this.Invoke(FileFound, new object[] { filePath });
                }
            }


            string[] dirPaths = { };
            try
            {
                dirPaths = Directory.GetDirectories(searchPath);
            }
            catch (System.UnauthorizedAccessException)
            { };

            foreach (var dirPath in dirPaths)
            {
                RecursiveSearch(dirPath, searchFilename, searchPattern, mre, token);
            }
        }
        #endregion
    }
}
