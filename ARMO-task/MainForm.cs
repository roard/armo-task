﻿using System;
using System.Windows.Forms;


using ARMO_task.Interfaces;
using ARMO_task.Utility;

namespace ARMO_task
{
    public partial class MainForm : Form, IMainFormView
    {
        private MainFormGUIState _buttonSearchState = MainFormGUIState.ReadyForSearch;
        private long _searchTimeElapsed = 0;
        private long _filesProcessed = 0;

        #region Events
        public event Action CommandSearchStart;
        public event Action CommandSearchPause;
        public event Action CommandSearchResume;
        #endregion

        #region Properties
        /// <summary>
        /// Возвращает текущие параметры поиска, введенные в форму
        /// </summary>
        public MainFormSearchParams SearchParams
        {
            get
            {
                MainFormSearchParams result = null;
                try
                {
                    result = new MainFormSearchParams(pathTextBox.Text, filenameTextBox.Text, targetContentTextBox.Text);
                }
                catch(Exception e)
                {
                    ShowError(e.ToString());
                }
                return result;
            }
            set
            {
                if (value != null)
                {
                    pathTextBox.Text = value.Path;
                    filenameTextBox.Text = value.Filename;
                    targetContentTextBox.Text = value.InnerPattern;
                }
            }
        }

        /// <summary>
        /// Возвращает ссыку на контрол DirectorySearcher из формы
        /// </summary>
        public DirectorySearcher DirectorySearcher
        {
            get
            {
                return directorySearcher;
            }
        }
        #endregion

        public MainForm()
        {
            InitializeComponent();
            searchTimer.Tick += OnTimerTick;
        }
        void IView.Show()
        {
            Application.Run(this);
        }

        #region Form events handling
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                pathTextBox.Text = folderBrowserDialog.SelectedPath;
            }
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            switch (_buttonSearchState)
            {
                case MainFormGUIState.ReadyForSearch:
                    CommandSearchStart?.Invoke();
                    break;
                case MainFormGUIState.Searching:
                    CommandSearchPause?.Invoke();
                    break;
                case MainFormGUIState.Paused:
                    CommandSearchResume?.Invoke();
                    break;
            }
        }
        private void newSearchButton_Click(object sender, EventArgs e)
        {
            GUIStateChange(MainFormGUIState.ReadyForSearch);
        }

        /// <summary>
        /// Инкрементит время поиска, приводит его в нужный формат, выводит на форму
        /// </summary>
        private void OnTimerTick(object sender, EventArgs e)
        {
            _searchTimeElapsed++;

            string secStr = (_searchTimeElapsed % 60).ToString("00");
            string hourStr = (_searchTimeElapsed / 60 % 24).ToString("00") + ":";

            long day = _searchTimeElapsed / 60 / 24;
            string dayStr = day == 0 ? "" : day.ToString() + "d. ";

            timerTextBox.Text = String.Concat(dayStr, hourStr, secStr);
        }
        #endregion

        #region DirrectorySearcher events handling
        /// <summary>
        /// Выводит в статус-бар путь к текущему обрабатываемому файлу
        /// </summary>
        /// <param name="filePath"></param>

        public void OnFileProcess(string filePath)
        {
            toolStripStatusLabel.Text = filePath;
            toolStripStatusLabel1.Text = (++_filesProcessed).ToString();
        }
        /// <summary>
        /// Добавляет в TreeView новый результат поиска
        /// </summary>
        /// <param name="filePath">Путь к результату поиска</param>

        public void OnNewSearchResult(string filePath)
        {
            TreeNode[] sameNodes = { };
            TreeNode thisStepNode = null;
            string[] dirs = filePath.Split('\\');
            string dirName;

            for (int i = 1; i < dirs.Length; i++)
            {
                dirName = dirs[i];

                if (thisStepNode == null)
                    sameNodes = treeView.Nodes.Find(dirName, false);
                else
                    sameNodes = thisStepNode.Nodes.Find(dirName, false);

                if (sameNodes.Length > 0)
                {
                    thisStepNode = sameNodes[0];
                }
                else
                {
                    treeView.BeginUpdate();

                    if (thisStepNode == null)
                        thisStepNode = treeView.Nodes.Add(dirName);
                    else
                        thisStepNode = thisStepNode.Nodes.Add(dirName);
                    thisStepNode.Name = dirName;

                    treeView.EndUpdate();
                }
            }
        }
        #endregion
        
        #region Public methods
        /// <summary>
        /// Переводит основные элементы формы в переданное состояние
        /// </summary>
        /// <param name="state">Целевое состояние</param>
        public void GUIStateChange(MainFormGUIState state)
        {
            _buttonSearchState = state;

            switch (state)
            {
                case MainFormGUIState.ReadyForSearch:
                    treeView.Nodes.Clear();
                    _searchTimeElapsed = 0;
                    _filesProcessed = 0;
                    buttonSearch.Enabled = true;
                    buttonSearch.Text = "Search";
                    SetInputGUIEnabled(true);
                    newSearchButton.Enabled = false;
                    break;
                case MainFormGUIState.Searching:
                    searchTimer.Start();
                    buttonSearch.Text = "Pause";
                    SetInputGUIEnabled(false);
                    newSearchButton.Enabled = false;
                    break;
                case MainFormGUIState.Paused:
                    searchTimer.Stop();
                    buttonSearch.Text = "Resume!";
                    newSearchButton.Enabled = true;
                    break;
                case MainFormGUIState.SearchEnded:
                    searchTimer.Stop();
                    buttonSearch.Text = "Resume!";
                    buttonSearch.Enabled = false;
                    newSearchButton.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Выводит в статус-бар переданный текст 
        /// </summary>
        /// <param name="err">Строка для выведения</param>
        public void ShowError(string err)
        {
            toolStripStatusLabel.Text = err;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Управляет свойством Enabled основных полей ввода формы
        /// </summary>
        /// <param name="value">Включены или выключены</param>
        private void SetInputGUIEnabled(bool value)
        {
            buttonBrowse.Enabled = value;
            pathTextBox.Enabled = value;
            filenameTextBox.Enabled = value;
            targetContentTextBox.Enabled = value;
        }
        #endregion
    }
}
