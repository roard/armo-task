﻿
namespace ARMO_task.Interfaces
{
    public interface IMainFormModel
    {
        void WriteSearchParams(MainFormSearchParams sParams);
        MainFormSearchParams ReadSearchParams();
    }
}
